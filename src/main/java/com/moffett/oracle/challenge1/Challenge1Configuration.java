package com.moffett.oracle.challenge1;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.core.Configuration;
import io.dropwizard.db.DataSourceFactory;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;

public class Challenge1Configuration extends Configuration {

    @JsonProperty("challenge1CustomAttribute")
    private String challenge1CustomAttribute;

    @Valid
    @NotNull
    private DataSourceFactory postgreSQLDatabase = new DataSourceFactory();

    public String getChallenge1CustomAttribute() {

        return challenge1CustomAttribute;
    }

    public void setChallenge1CustomAttribute(String challenge1CustomAttribute) {

        this.challenge1CustomAttribute = challenge1CustomAttribute;
    }

    @JsonProperty("PostgreSQLDatabase")
    public void setDataSourceFactory(DataSourceFactory factory) {

        this.postgreSQLDatabase = factory;
    }

    @JsonProperty("PostgreSQLDatabase")
    public DataSourceFactory getDataSourceFactory() {

        return postgreSQLDatabase;
    }
}
