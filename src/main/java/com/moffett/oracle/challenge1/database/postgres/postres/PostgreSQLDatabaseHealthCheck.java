package com.moffett.oracle.challenge1.database.postgres.postres;

import com.codahale.metrics.health.HealthCheck;

public class PostgreSQLDatabaseHealthCheck extends HealthCheck {

    @Override
    protected Result check() throws Exception {

        return Result.healthy();
    }
}
