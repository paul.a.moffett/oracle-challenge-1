package com.moffett.oracle.challenge1.database.postgres.postres;

import org.jdbi.v3.core.Jdbi;

public class PostgreSQLDatabaseUserResource {

    private final Jdbi jdbi;

    public PostgreSQLDatabaseUserResource(Jdbi jdbi) {

        this.jdbi = jdbi;
    }
}