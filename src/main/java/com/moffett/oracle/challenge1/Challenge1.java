package com.moffett.oracle.challenge1;

import io.dropwizard.migrations.MigrationsBundle;
import com.moffett.oracle.challenge1.database.postgres.postres.PostgreSQLDatabaseHealthCheck;
import com.moffett.oracle.challenge1.database.postgres.postres.PostgreSQLDatabaseUserResource;
import io.dropwizard.core.Application;
import io.dropwizard.core.Configuration;
import io.dropwizard.core.setup.Bootstrap;
import io.dropwizard.core.setup.Environment;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.jdbi3.JdbiFactory;
import org.jdbi.v3.core.Jdbi;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 */
public class Challenge1 extends Application<Challenge1Configuration> {

    private static final Logger LOGGER = LoggerFactory.getLogger(Challenge1.class);

    /**
     * Entrypoint for our Java Application. Consider Dropwizard documentation for more
     * information on command line parameters and running options.
     *
     * @param args Command line parameter arguments
     */
    public static void main(String[] args) throws Exception {

        LOGGER.info("Attempting to run Dropwizard server");
        new Challenge1().run(args);
    }

    /**
     * Run our migrations logic.
     *
     * @param bootstrap Pre-run Configuration and Logic.
     */
    @Override
    public void initialize(Bootstrap<Challenge1Configuration> bootstrap) {

        // Logging not initialized, outputting to confirm we're working, might want to remove

        System.out.println("Attempting to run initialization logic");
        System.out.println("Attempting to add migrations bootstrapping logic");

        // We want to run our migrations before we start running the application. We overwrite the default
        // migrations directory and file type used as Java args at runtime by overriding below:
        //
        // migrationFile: /config/migrations.yml

        bootstrap.addBundle(new MigrationsBundle<Challenge1Configuration>() {

            @Override
            public String getMigrationsFileName() {

                return "/config/migrations.yml";
            }

            @Override
            public DataSourceFactory getDataSourceFactory(Challenge1Configuration configuration) {

                return configuration.getDataSourceFactory();
            }
        });
    }

    /**
     * We should aim to keep this method clean. Factories, CDI and other mechanisms can be
     * used to hide complex logic and keep the application easy to maintain and read.
     *
     * @param configuration the parsed {@link Configuration} object
     * @param environment   the application's {@link Environment}
     * @throws Exception General Exception.
     */
    @Override
    public void run(Challenge1Configuration configuration, Environment environment) throws Exception {

        LOGGER.info("Running Dropwizard server");
        LOGGER.debug("Configuration: {}", configuration);

        LOGGER.debug("Attempting to register database factory");
        final JdbiFactory factory = new JdbiFactory();
        final Jdbi jdbi = factory.build(environment, configuration.getDataSourceFactory(), "postgresql");
        environment.jersey().register(new PostgreSQLDatabaseUserResource(jdbi));

        LOGGER.debug("Attempting to register database health check");
        environment.healthChecks().register("PostgreSQLDatabase", new PostgreSQLDatabaseHealthCheck());
    }
}