package com.moffett.oracle.challenge1;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestMain {

    @Test
    void testChallenge1() {

        Challenge1 challenge1 = new Challenge1();
        Assertions.assertEquals(Challenge1.class, challenge1.getClass());
    }
}