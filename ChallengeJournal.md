# Oracle Challenge 1

## Challenge Overview

Create a small web application with a ReactJS UI and a Dropwizard Backend REST API service, runnable within docker containers and deployable to Oracle Cloud Infrastructure. Include considerations for testing, logging, monitoring (might want to consider liveness/readiness polling and probes if I have time) etc.

## Assumptions, Notes, Process etc

 - I am conscious of the fact that my repo is set to `public`, however, I didn't want to start asking for SSH keys, in reality, we would want to lock this repo and use SSH keys for at least 1 layer of authentication. Also, in practice we might seek to fork each repo and lock down the main repo, only allowing merge to the main repo via fork merges, as a layer of security for reviews and pipeline checks. 
 - We start with a basic templated outline Java Application from IntelliJ, this includes a small "Hello World" output from a `main` method. We have selected the latest JDK which is supported by our build automation tool of choice which is gradle. The basic outline also generates a `build.gradle` file with basic logic to reach the `maven` repositories. We also have a small outline for test logic using `junit`. 
 - I have created a new directory `functionalTest`, the aim here will be to have tests which run the application within the docker container, also to run any related systems such as the database, or message queue system etc. 
 - Atm our docker image tag is defined as latest, we may want to consider updating that to gradle getVersion using nebula plugin also, and creating unique tags to allows multiple devs to share work and publish to a remote repo
 - It would appear that Dropbox is quite annotation and CDI heavy, and also takes full advantage of a configuration object. I will likely seek to run the `dropwizard-jdbi3` and use PostgreSQL running its own instance. Locally this would be docker, FVT's will load each container. On OCI, I'm not sure yet, but will have to spin it up and see.
 - It would appear that the Dropwizard the config file location is specified as a command line argument on the entryPoint
 - Interesting host links for health and metrics on 0.0.0.0:8080 by default with limited code so far
 - Challenge instructions state to use different images for UI and Services, therefore I'll separate to a different repo

## Tasks Needing Done Pending Time

 - Add Spotless with Google style config for linting
 - Add SonarCube if I've time
 - Add Artifactory if I've time
 - Add some kind of CVE scanning, Whitesource etc
 - Add CICD pipeline logic to run Spotless, SonarCube, Publish to Artifactory, CVE Scanning etc
 - I'm not sure if I'll get time for the FVT's, but the idea is simply to add logic to build.gradle such that I can run a new test suit with `./gradlew build functionalTest`, those tests should start the various containers and therefore allow assertions in a functional or "real world" context.
 - Logic needs added to build.gradle to build for production vs local to select the related config from `/config`