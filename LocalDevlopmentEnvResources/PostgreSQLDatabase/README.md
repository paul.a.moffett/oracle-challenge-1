## Dev Instructions

Build the docker container and run. It might be good to not run as -d, and leave in a session of its own to view what's happening:

```bash
docker build . -t postgresql1
docker run -p 5432:5432 docker.io/library/postgresql1
```