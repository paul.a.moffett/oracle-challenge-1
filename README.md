# Oracle Challenge 1

A journal of the challenge, assumptions, notes and process can be found [here](https://gitlab.com/paul.a.moffett/oracle-challenge-1/-/blob/main/ChallengeJournal.md)

## Note on Technology Choice

The requirement is to use Dropwizard. We will want to stay as close to that as possible to keep the application lightweight and avoid possible conflicts. This means we will want to use all the features of Dropwizard were reasonable, reliable and possible. Therefore, we will seek to take advantage of the Dropwizard docs and build close to their guidelines. We will therefore inherit the Dropwizard choices for logging, metrics, dependency injection, REST services etc.

I have decided to use Gradle in the interest of time, as I need to have the capability to create the docker container on the fly, which includes copying in the Dropwizard config file and exposing ports etc, all tasks I can quickly do with Gradle. However, I understand from the Dropwizard docs that Maven is the tool of choice and will likely be used by everyone else. 

### Authentication

It would appear that Dropwizard offers support for various authentication options including basic authentication, digest authentication, and OAuth 2.0. Also, it appears Dropwizard integrates with several third-party authentication and authorization libraries, such as Shiro and Apache Knox, which can be used to add more advanced security features.

## Development Guide: Local

 - You will need to create the PostgreSQLDatabase container for local development and run the container. More information can be found in this repo at `LocalDevlopmentEnvResources/PostgreSQLDatabase/`
 - You will need to create the main Dropwizard App Docker image and run the container using the instructions below, this expose 2 ports as described below and the application should start
 - You will need to create a bash session into the Application container and run the DB migrations manually. This should be replaced with Managed service. See: https://www.dropwizard.io/en/latest/manual/migrations.html

### Build Docker Image and Start Container for Dropwizard App

You will need to download, install, run and authenticate to Docker Desktop. We will use the `bmuschko` docker plugin defined in our `build.gradle` to create the docker file. You will also need to authenticate your command line session docker service also, on mac or linux if will be as follows:

Possible error during build:

```bash
Could not build image: pull access denied for dockerfile/java, repository does not exist or may require 'docker login': denied: requested access to the resource is denied
```

Authenticating your Docker service from command line client:

```bash
docker login
```

You should be able to run the following to create your image:

```bash
./gradlew build test dockerBuildImage
```

You can exclude the tests with `-x test`, but you might want to run the various tests before building:

```bash
./gradlew build -x test dockerBuildImage
```

Your command line output should give you a new image tag reference. You can use `docker inspect {imageTag}` to see the configured entryPoint, architecture etc. You can use `docker image list` if you can't find your latest build tag.

More information on the currently used Docker plugin at: https://bmuschko.github.io/gradle-docker-plugin/current/user-guide/

You might need to bind your Docker ports, as follows:

```bash
docker run -dt -p 8080:8080 -p 8081:8081 com.moffett.oracle.challenge1:latest
```

You will then be able to access on `127.0.0.1:8080` and `127.0.0.1:8081`

### Health, Readiness, Metrics etc.

We expose 2 ports through the application, 8080 for the main application, and 8081 for health, readiness and metrics etc. They should be exposed on `localhost` `127.0.0.1`.

## Development Guide: Oracle Cloud Infrastructure



### Note on Docker image build architecture types 

You should take into consideration that the dockerImage created locally or on Gitlabs CICD Jobs System will need to match with the architecture type for whichever cloud infrastructure we might use. Creating images on Apples own new chip will have a different architecture type than creating the image on RedHat. Your cloud infrastructure might be configured to run certain types.

You can define the type of architecture you're creating and working with using:

```bash
export DOCKER_DEFAULT_PLATFORM=linux/arm64
```

or

```bash
export DOCKER_DEFAULT_PLATFORM=linux/amd64
```